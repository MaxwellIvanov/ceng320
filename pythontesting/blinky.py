#!/usr/bin/env python3

import RPi.GPIO as GPIO
from time import sleep

LED_PIN = 12
#LED_PIN = 13
#LED_PIN = 14
#LED_PIN = 15

GPIO.setwarnings(False)

#GPIO.setmode(GPIO.BOARD)
GPIO.setmode(GPIO.BCM)


GPIO.setup(LED_PIN, GPIO.OUT)

GPIO.output(LED_PIN, GPIO.HIGH)

sleep(1)

GPIO.output(LED_PIN, GPIO.LOW)

sleep(1)

GPIO.cleanup()