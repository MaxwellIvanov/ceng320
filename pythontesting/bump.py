#!/usr/bin/env python3

import RPi.GPIO as GPIO

BUTTON_IO = 10
#BUTTON_IO = 11

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON_IO, GPIO.IN,pull_up_down=GPIO.PUD_DOWN)

while True:
        if GPIO.input(BUTTON_IO) == GPIO.HIGH:
                print("Button HIGH")
        else:
                print("Button LOW")