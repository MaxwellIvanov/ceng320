
	.equ	GPSET0, 0x1C
	.equ	GPCLR0, 0x28

	.equ	BIT4MASK, 0b1111
	.equ	IOBITOFFSET, 12

.text

	.global setlights

setlights:
	
	adr	x0, LIGHTCOUNTER
	ldr	x0, [x0]

	adr	x1, GPIOPTR
	ldr	x1, [x1]


	and	x2, x0, BIT4MASK

	mvn	x3, x0

	and	x3, x3, BIT4MASK	

	add	x4, x1, GPCLR0
	add	x5, x1, GPSET0

	lsl	x2, x2, IOBITOFFSET
	lsl	x3, x3, IOBITOFFSET

	str	w2, [x4]
	str	w3, [x5]

	ret
