# SDSM&T CENG320 Spring 2024 - Group Project

Team #7

Maxwell Ivanov and Craig Berens

## Description

The purpose of the project is to implement a four LED binary counter with an increment and decrement button using ARM64 assembly on a Raspberry Pi 3B.

## Hardware Configuration

We were provided a Raspberry Pi 3B v1.2.  See pinout.txt

LEDs

Resistors

Push Buttons - Normally Open

Jumper Wires

The GPIO on the Rasperry PI 3B v1.2 has supporting circuitry set pull up/down resistors for the button inputs and to drive the LED.

The cathode of the LED was connected to a 330 ohm current limiting resistor put in series to the 3.3 volt supply.  The anode connected to the GPIO pin on the Rasberry PI.  By setting the output low this would turn on the LED.

GPIO 9 through 27 are believed to be configured to pull down to 0V by default.
The buttons were connected to the postive rail and the pins listed below.

The following Rasberry Pi pins were used:
|Pin  |Function |Purpose                       | Wire   |
|-----|---------|------------------------------|--------|
| 1  | 3.3 VDC | Positive rail on the board   | Red |
| 6  | Ground  | Negative rail on the board   | Black |
| 8  | GPIO 14 | Output - Bit 2 (3rd bit) LED | Orange |
| 10 | GPIO 15 | Output - Bit 3 (4th bit) LED | Yellow |
| 19 | GPIO 10 | Input - Increment Button     | Brown |
| 23 | GPIO 11 | Input - Decrement Button     | White |
| 32 | GPIO 12 | Output - Bit 0 (1st bit) LED | Gray |
| 33 | GPIO 13 | Output - Bit 1 (2nd bit) LED | Green |


The blue and purple wires connect the buttons to the positive rail.



## Software Environment

Rasperry Pi OS 64 bit - Based on Debian 12

Linux rpi-group7 6.6.28+rpt-rpi-v8 #1 SMP PREEMPT Debian 1:6.6.28-1+rpt1 (2024-04-22) aarch64 GNU/Linux

gcc (Debian 12.2.0-14) 12.2.0

Bootstrap c code was provided.  See setup_io.c

setup_io.c memory maps the GPIO address space into the process space.  This attempts to provide memory safety that we only access the GPIO controller memory space.  This prevents our program from unintentiionally reading or writing to random places in memory.  See results and conclusions regarding unpriviledged GPIO acess.

Python 3.11.2

Python module RPi.GPIO 0.7.1a4

## Software Implementation

Very rough python scripts were developed to test the LED and buttons functionality.  See pythontesting folder.


The counter program was compiled the Makefile

make

then ran:

./counter

## Bugs and Troubleshooting
There was an intial problem with burnt LEDs and a bad wire.  Using a multimeter and the test python programs, the hardware problems were resolved.

## Results and Conclusions

Unpriviledged GPIO Access
While writing the testing python scripts the mechanism for how the scripts accessed the GPIO was unclear.  After doing some research there is a driver in Raspberry Pi OS, rpi-gipomem that accesses the GPIO memory with a device /dev/gpiomem.  /dev/mem by default is only accessible by root or with sudo.  /dev/gpiomem is readable and writeable by any user in the "gpio" group.  Allowing users without root access to interact with the GPIO.  This may be a safer option for the setup_io.c in the future rather than running the program as root or with sudo.

```console
craigb@rpi-group7:~/ceng320 $ dmesg -H | grep gpio
[  +0.106660] rpi-gpiomem 3f200000.gpiomem: window base 0x3f200000 size 0x00001000
[  +0.007499] rpi-gpiomem 3f200000.gpiomem: initialised 1 regions as /dev/gpiomem
craigb@rpi-group7:~/ceng320 $ ls -alrth /dev/gpiomem
crw-rw---- 1 root gpio 238, 0 Apr 28 00:01 /dev/gpiomem
craigb@rpi-group7:~/ceng320 $ groups
craigb adm dialout cdrom sudo audio video plugdev games users input render netdev lpadmin gpio i2c spi
craigb@rpi-group7:~/ceng320 $ diff setup_io.c setup_io_gpiomem.c
19,20c19,20
<   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
<     printf("can't open /dev/mem \n");
---
>   if ((mem_fd = open("/dev/gpiomem", O_RDWR|O_SYNC) ) < 0) {
>     printf("can't open /dev/gpiomem \n");
craigb@rpi-group7:~/ceng320 $
```

https://stackoverflow.com/questions/31622586/how-gpio-is-mapped-in-memory